<section class="Gallery">
    <h2>
        <small>Как мы работаем?</small>
        <span>Портфолио</span>
    </h2>
    <p>
        С 2010 по 2014 год ООО «АВТОБАН» выполняло работы по договорам с бюджетными организациями города
        и области, посредством участия в электронных аукционах, согласно 94 ФЗ, по договорам субподряда с государ-
        ственными предприятиями области а также сотрудничало с различными коммерческими предприятиями.
    </p>
    <a class="Go-PDF" href="/img/pdf/album.pdf" download>Скачать наше портфолио&nbsp;(PDF)</a>
    <article class="Report">
        <h3>
            Ремонт дворовых территорий многоквартирных домов в г. Владимире
            <span>Заказчик: МКУ города Владимира «Благоустройство»</span>
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g1.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g2.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g3.jpg" alt=""/></div>
            </li>
        </ul>

    </article>
    <article class="Report">
        <h3>
            Ремонт автодорожного покрытия и заделка деформации на федеральной трассе «Волга М-7»
            <span> Заказчик: ОАО ДЭП-7</span>
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g4.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g5.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g6.jpg" alt=""/></div>
            </li>
        </ul>

    </article>
    <article class="Report">
        <h3>
            Капитальный ремонт асфальтобетонного покрытия территории учебного корпуса Владимирского государственного
            университета
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g7.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g8.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g9.jpg" alt=""/></div>
            </li>
        </ul>

    </article>
    <article class="Report">
        <h3>
            Строительство участка автомобильной дороги Владимир-Улыбышево-Коняево
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g10.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g11.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g12.jpg" alt=""/></div>
            </li>
        </ul>


    </article>
    <article class="Report">
        <h3>
            Устройство покрытия из щебеночномастичной асфальтобетонной смеси на участке автомобильной дороги Владимир –
            Гусь-Хрустальный - Тума
             <span>Заказчик: Государственное бюджетное
            учреждение Владимирской области
            «Управление автомобильных дорог Владимирской
            области» </span>
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g13.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g14.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g15.jpg" alt=""/></div>
            </li>
        </ul>

    </article>

    <article class="Report">
        <h3>
            Ремонт дорожного покрытия
            в г. Гусь-Хрустальный
            <span>Заказчик: Администрация г. Гусь-Хрустальный
        </span>
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g16.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g17.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g18.jpg" alt=""/></div>
            </li>
        </ul>

    </article>

    <article class="Report">
        <h3>
            Ремонт дорожного покрытия
            в городе Владимире
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g19.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g20.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g21.jpg" alt=""/></div>
            </li>
        </ul>
    </article>
    <article class="Report">
        <h3>
            Выполнение ямочного ремонта дорожного
            покрытия в городе Коврове
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g22.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g23.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g24.jpg" alt=""/></div>
            </li>
        </ul>

    </article>
    <article class="Report">
        <h3>
            Ремонт дорожного покрытия
            автозаправочных станций
            ООО ТД «Альфа-трейд» («Газпром»)
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g25.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g26.jpg" alt=""/></div>
            </li>
        </ul>
    </article>
    <article class="Report">
        <h3>
            Восстановление разрытий ОАО «ТГК-6»
            по городу Владимиру
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g27.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g28.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g29.jpg" alt=""/></div>
            </li>
        </ul>
    </article>
    <article class="Report">
        <h3>

            Выполнение полного комплекса работ по
            благоустройству и озеленению
            территории новых домов
            по ул. Западный пр-д, д. 8 и ул. Пугачева, д. 62
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g30.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g31.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g32.jpg" alt=""/></div>
            </li>
        </ul>
    </article>
    <article class="Report">
        <h3>
            Строительство парковки и ямочный ремонт
            дорожного покрытия на молочном заводе
            «Юнимилк» («Danone») г. Владимир
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g33.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g34.jpg" alt=""/></div>
            </li>
        </ul>
    </article>
    <article class="Report">
        <h3>
            Устройство дорожного покрытия
            к территории завода «Hame»
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g35.jpg" alt=""/></div>
            </li>
        </ul>
    </article>
    <article class="Report">
        <h3>
            Выполнение работ по благоустройству
            производственной территории
            из дорожных плит
        </h3>
        <ul>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g36.jpg" alt=""/></div>
            </li>
            <li>
                <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                    <img
                        src="/img/pic/g37.jpg" alt=""/></div>
            </li>
        </ul>

    </article>


</section>