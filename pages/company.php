<?php

$Images = [
    'Road-Works' => [
        'slider-1' => [

            '1',
            '2',
            '3',
            '4',
            '5',
            '6'
        ],
        'slider-2' => [
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
        ],
        'slider-3' => [
            '13',
            '14',
            '15',
            '16',
            '17',
            '18'
        ],
        'slider-4' => [
            '19',
            '20',
            '21',
            '22',
            '23',
            '24'
        ],
        'slider-5' => [
            '25',
            '26'
        ]
    ],
    'Rent' => [
        'slider-1' => [

            '27',
            '28',
            '29',
            '30',
            '31',
            '32'
        ],
        'slider-2' => [
            '33',
            '34',
            '35',
            '36',
            '37',
            '38',
        ],
        'slider-3' => [
            '39',
            '40',
            '41'
        ]
    ],
    'Supervision' => [
        'slider-1' => [

            '42',
            '43',
            '44',
            '45',
            '46',
            '47'
        ],
        'slider-2' => [
            '48',
            '49',
            '50',
            '51',
            '52',
            '53',
        ],
        'slider-3' => [
            '54',
            '55'
        ]

    ],
    'Delivering' => [
        'slider-1' => [

            '56',
            '57',
            '58'
        ],

    ],
    'beautification' => [
        'slider-1' => [

            '59',
            '60',
            '61',
            '62',
            '63',
            '64'
        ],
        'slider-2' => [
            '65',
            '66',
            '67',
            '68',
            '69',
            '70',
        ],
        'slider-3' => [
            '71',
            '72',
            '73',
            '74',
            '75',
            '76'
        ],
        'slider-4' => [
            '77',
            '78',
            '79',
            '80',
            '81',
            '82'
        ],

    ],
    'greenery' => [
        'slider-1' => [

            '83',
            '84',
            '85',
            '86',
            '87',
            '88'
        ],
        'slider-2' => [
            '89',
            '90',
            '91'
        ]
    ],
    'garbage' => [
        'slider-1' => [

            '92',
            '93',
            '94',
            '95',
            '96',
            '97'
        ],
        'slider-2' => [
            '98',
            '99',
            '100'
        ]
    ]


];


?>

<div class="Company">
    <!--HEADER-OFFER-->
    <div class="Header-Offer">
        <div class="Background"></div>
        <section class="Content">
            <h1><span>Дорожно строительные работы</span><span>любой сложности
                </span>
            </h1>
            <a href="/gallery">Посмотреть наше портфолио</a>
        </section>
    </div>


    <!--SERVICES-->
    <section class="Services JS-Tabs">
        <h2 class="Title JS-Material">
            <small>Чем мы занимаемся?</small>
            <span>Наши услуги</span>
        </h2>
        <ul class="Nav JS-Tabs-Navigation">
            <li class="JS-Tab Active JS-TouchButton" data-href="Tab-1">
                <button>Дорожно&ndash;строительные работы</button>
            </li>
            <li class="JS-Tab JS-TouchButton" data-href="Tab-6">
                <button>Озеленение. Тепличное хозяйство</button>
            </li>
            <li class="JS-Tab JS-TouchButton" data-href="Tab-3">
                <button>Технический надзор за строительством</button>
            </li>
<!--            <li class="JS-Tab JS-TouchButton" data-href="Tab-4">-->
<!--                <button>Доставка стройматериалов</button>-->
<!--            </li>-->
<!--            <li class="JS-Tab JS-TouchButton" data-href="Tab-5">-->
<!--                <button>Благоустройство территории</button>-->
<!--            </li>-->

            <li class="JS-Tab JS-TouchButton" data-href="Tab-7">
                <button>Сбор, вывоз и&nbsp;утилизация мусора</button>
            </li>
            <li class="JS-Tab JS-TouchButton" data-href="Tab-8">
                <button>Зимняя уборка. Вывоз снега</button>
            </li>
            <li class="JS-Tab JS-TouchButton" data-href="Tab-2">
                <button>Аренда спецтехники</button>
            </li>
        </ul>

        <div class="JS-Tabs-Content">
            <div class="Views-Wrap JS-Carousel" data-tab='Tab-1'>

                <ul class="Views JS-Carousel-List">
                    <?php foreach ($Images['Road-Works'] as $Item) { ?>


                        <li class="List JS-Carousel-Item">
                            <ul>
                                <?php foreach ($Item as $item_image) { ?>

                                    <li>
                                        <div class="View-Wrap">
                                            <div class=" View">
                                                <img src="/img/carousel/<?= $item_image ?>.jpg" alt=""/>
                                            </div>
                                        </div>
                                    </li>


                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
                </ul>


            </div>
            <div class="Views-Wrap JS-Carousel" data-tab='Tab-2'>

                <ul class="Views JS-Carousel-List">
                    <?php foreach ($Images['Rent'] as $Item) { ?>


                        <li class="List JS-Carousel-Item">
                            <ul>
                                <?php foreach ($Item as $item_image) { ?>

                                    <li>
                                        <div class="View-Wrap">
                                            <div class=" View">
                                                <img
                                                    src="/img/carousel/<?= $item_image ?>.jpg" alt=""/></div>
                                        </div>
                                    </li>


                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
                </ul>


            </div>
            <div class="Views-Wrap JS-Carousel" data-tab='Tab-3'>

                <ul class="Views JS-Carousel-List">
                    <?php foreach ($Images['Supervision'] as $Item) { ?>


                        <li class="List JS-Carousel-Item">
                            <ul>
                                <?php foreach ($Item as $item_image) { ?>

                                    <li>
                                        <div class="View-Wrap">
                                            <div class=" View">
                                                <img
                                                    src="/img/carousel/<?= $item_image ?>.jpg" alt=""/></div>
                                        </div>
                                    </li>


                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
                </ul>


            </div>
<!--            <div class="Views-Wrap JS-Carousel" data-tab='Tab-4'>-->
<!---->
<!--                <ul class="Views JS-Carousel-List">-->
<!--                    --><?php //foreach ($Images['Delivering'] as $Item) { ?>
<!---->
<!---->
<!--                        <li class="List JS-Carousel-Item">-->
<!--                            <ul>-->
<!--                                --><?php //foreach ($Item as $item_image) { ?>
<!---->
<!--                                    <li>-->
<!--                                        <div class="View-Wrap">-->
<!--                                            <div class=" View">-->
<!--                                                <img-->
<!--                                                    src="/img/carousel/--><?//= $item_image ?><!--.jpg" alt=""/></div>-->
<!--                                        </div>-->
<!--                                    </li>-->
<!---->
<!---->
<!--                                --><?php //} ?>
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!--                    --><?php //} ?>
<!--                </ul>-->
<!---->
<!---->
<!--            </div>-->
<!--            <div class="Views-Wrap JS-Carousel" data-tab='Tab-5'>-->
<!---->
<!--                <ul class="Views JS-Carousel-List">-->
<!--                    --><?php //foreach ($Images['beautification'] as $Item) { ?>
<!---->
<!---->
<!--                        <li class="List JS-Carousel-Item">-->
<!--                            <ul>-->
<!--                                --><?php //foreach ($Item as $item_image) { ?>
<!---->
<!--                                    <li>-->
<!--                                        <div class="View-Wrap">-->
<!--                                            <div class=" View">-->
<!--                                                <img-->
<!--                                                    src="/img/carousel/--><?//= $item_image ?><!--.jpg" alt=""/></div>-->
<!--                                        </div>-->
<!--                                    </li>-->
<!---->
<!---->
<!--                                --><?php //} ?>
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!--                    --><?php //} ?>
<!--                </ul>-->
<!---->
<!---->
<!--            </div>-->
            <div class="Views-Wrap JS-Carousel" data-tab='Tab-6'>

                <ul class="Views JS-Carousel-List">
                    <?php foreach ($Images['greenery'] as $Item) { ?>


                        <li class="List JS-Carousel-Item">
                            <ul>
                                <?php foreach ($Item as $item_image) { ?>

                                    <li>
                                        <div class="View-Wrap">
                                            <div class="View">
                                                <img
                                                    src="/img/carousel/<?= $item_image ?>.jpg" alt=""/></div>
                                        </div>
                                    </li>


                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
                </ul>


            </div>
            <div class="Views-Wrap JS-Carousel" data-tab='Tab-7'>

                <ul class="Views JS-Carousel-List">
                    <?php foreach ($Images['garbage'] as $Item) { ?>


                        <li class="List JS-Carousel-Item">
                            <ul>
                                <?php foreach ($Item as $item_image) { ?>

                                    <li>
                                        <div class="View-Wrap">
                                            <div class="View">
                                                <img
                                                    src="/img/carousel/<?= $item_image ?>.jpg" alt=""/></div>
                                        </div>
                                    </li>


                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
                </ul>

            </div> <div class="Views-Wrap JS-Carousel" data-tab='Tab-8'>

                <ul class="Views JS-Carousel-List">
                    <?php foreach ($Images['garbage'] as $Item) { ?>


                        <li class="List JS-Carousel-Item">
                            <ul>
                                <?php foreach ($Item as $item_image) { ?>

                                    <li>
                                        <div class="View-Wrap">
                                            <div class="View">
                                                <img
                                                    src="/img/carousel/<?= $item_image ?>.jpg" alt=""/></div>
                                        </div>
                                    </li>

                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
                </ul>

            </div>
        </div>


    </section>
    <!--CAR-PARK-->
    <section id="car-park" class="Car-Park">
        <h2>Наш автопарк</h2>
        <ul class="Park-List">
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#grader"></use>
                    </svg>
                </div>
                <h4>Автогрейдер</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#tractor"></use>
                    </svg>
                </div>
                <h4>Тракторы</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#tiptruck"></use>
                    </svg>
                </div>
                <h4>Самосвалы</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#washer"></use>
                    </svg>
                </div>
                <h4>Уборочная, поливо-моечная техника</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#loader"></use>
                    </svg>
                </div>
                <h4>Фронтальные погрузчики</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#steamroller"></use>
                    </svg>
                </div>
                <h4>Асфальтоукладчик, дорожные и грунтовые катки, фреза, автогудронатор</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#crane"></use>
                    </svg>
                </div>
                <h4>Кран-манипулятор</h4>
            </li>
            <li>
                <div class="Picture">
                    <div class="Lock">
                        <div>
                            <span class="First"></span>
                            <span></span>
                            <span class="Second"></span>
                        </div>
                        <div></div>
                        <div>
                            <span class="Third"></span>
                            <span></span>
                            <span class="Fourth"></span>
                        </div>
                    </div>
                    <svg>
                        <use xlink:href="#truck"></use>
                    </svg>
                </div>
                <h4>Трал</h4>
            </li>

        </ul>
    </section>
    <section class="About">
        <h2>О компании</h2>

        <p>
            Группа компаний «АВТОБАН» длительное время занимается дорожным строительством, земляными работами
            различной сложности и благоустройством территорий.
            Имея за плечами своих работников многолетний практический опыт дорожных работ, используя в работе передовую
            технику, а именно: дорожные катки и асфальтоукладчик от общепризнанного лидера в производстве дорожной
            техники, компании DYNAPAC, владея обширным парком специальной техники и механизмов, начиная от
            грузовиков-самосвалов различной грузоподъемности, тракторов, погрузчиков, грейдеров, бульдозеров,
            экскаваторов и заканчивая дорожной фрезой, применяя новейшие технологии дорожного строительства, группа
            компаний «АВТОБАН» решает практически весь спектр задач по укладке качественного дорожного покрытия и
            ремонта.

        </p>

        <h2>Сильные стороны</h2>
        <ul>
            <li>наличие допуска СРО на выполнение работ различной сложности</li>
            <li>наличие лицензии на обращение с отходами</li>
            <li>собственная техника — более 30 единиц</li>
            <li>собственная ремонтная база</li>
            <li>высококвалифицированные механизаторы и рабочие с большим опытом работы</li>
            <li>профессиональный штат инженерно-технических работников</li>
            <li>выполнение работ только качественными материалами</li>
            <li>контроль качества на всех этапах работы</li>
            <li>свой штат специалистов, выполняющих технический надзор с использованием поверенных приборов и
                инструментов, в том числе в лабораторных условиях
            </li>
            <li>рекомендации заказчиков — как государственных и муниципальных структур, так и коммерческих
                предприятий.
            </li>
        </ul>
    </section>

</div>