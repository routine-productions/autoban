<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Автобан</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/fav/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/fav/manifest.json">
    <link rel="mask-icon" href="/img/fav/safari-pinned-tab.svg" color="#ff5400">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800italic,800&subset=latin,cyrillic'
        rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
<?php require __DIR__ . "/img/sprite.svg"; ?>

<?php

require_once __DIR__ . '/libs/Mobile_Detect.php';
$Detect = new Mobile_Detect;
$Dir_Images = __DIR__ . '/images/';
?>

<div class="Header-Wrap">
    <?php require_once __DIR__ . '/modules/header.php'; ?>
</div>
<main>
    <?php
    if ($_SERVER['REQUEST_URI'] == '/') {
        require __DIR__ . '/pages/company.php';

    } else {
        require __DIR__ . '/pages/' . $_SERVER['REQUEST_URI'] . '.php';
    }
    ?>

</main>
<footer class="Site-Footer">
    <?php require_once __DIR__ . '/modules/footer.php'; ?>

</footer>


<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="index.min.js"></script>

<body>


</html>

