<div class="Footer-List">
    <div class="Info">
        <a class="Logo" href="/">
            <svg>
                <use xlink:href="#logo"></use>
            </svg>
            <dl>
                <dt>АВТОБАН</dt>
                <dd>Группа компаний</dd>
            </dl>
        </a>
        <dl>
            <dt>© ООО «Автобан», 2016&nbsp;г.</dt>
            <dd>Аренда спецтехники, проведение дорожных и&nbsp;земельных работ, озеленение и&nbsp;вывоз мусора</dd>
        </dl>
    </div>
    <div class="Address">
        <dl>


            <dt>Адрес</dt>
            <dd>

                ул.&nbsp;Добросельская, 164-а
                г.&nbsp;Владимир

            </dd>
            <dd>Владимирская обл.

            </dd>
            <dd> 600031,</dd>
            <dd>Россия.</dd>
        </dl>
    </div>
    <div class="Phones">
        <dl>
            <dt>Для связи</dt>
            <dd>+7&nbsp;(920)&ndash;920&ndash;00&ndash;55</dd>
            <dd>тел. факс.&nbsp;(4922)&nbsp;21&ndash;95&ndash;51</dd>
            <dd>E-mail:<a href="mail-to:kds-33@mail.ru"> kds-33@mail.ru</a></dd>
        </dl>
    </div>
    <div class="Made-By">
        <dl>
            <dt>Продвижение:</dt>
            <dd>
                <a class="Progress-Time" href="http://progress-time.ru/"><span>Progress-Time</span>
                    <small>маркетинговое агенство</small>
                </a>
            </dd>
        </dl>
        <dl>
            <dt>Разработка:</dt>
            <dd>
                <a href="http://routine.bunker-lab.com/">
                    <svg>
                        <use xlink:href="#rootine"></use>
                    </svg>
                </a>
            </dd>
        </dl>
    </div>
</div>