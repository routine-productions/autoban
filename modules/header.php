<header class="Site-Header">
    <a href="/" class="Logo">
        <svg>
            <use xlink:href="#logo"></use>
        </svg>
        <dl>
            <dt>АВТОБАН</dt>
            <dd>Группа компаний</dd>
        </dl>
    </a>
    <ul class="Header-Nav">
        <li class="JS-Current-Links"><a href="/gallery">портфолио</a></li>
        <li class="JS-Current-Links"><a href="/vacancies">вакансии</a></li>
        <li>
            <a href="/#car-park" data-scroll-point-duration='600'  class="JS-Scroll-Button">автопарк</a>
        </li>
        <li class="JS-Current-Links">
            <a href="/catalog">аренда</a>
        </li>
        <li class="JS-Current-Links"><a href="/contacts">контакты</a></li>
    </ul>


    <div class="Phones">
        <span>+7&nbsp;(920)&nbsp;920&ndash;00&ndash;55</span>
        <span>+7&nbsp;(4922)&nbsp;46&ndash;20&ndash;28</span>
    </div>

</header>