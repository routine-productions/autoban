/*
 * Copyright (c) 2015
 * Routine JS - Carousel
 * Version 0.2.2 Beta
 * Created 2016.02.09
 * Author Bunker Labs
 */

$.fn.JS_Easy_Carousel = function (Settings) {

    $(this).each(function (Carousel_Key, Carousel) {
        var Methods = {};
        var Object = $.extend({
            // To customize
            List: $(Carousel).find('.JS-Carousel-List'),
            Item: '.JS-Carousel-Item',
            Next: $('<div/>', {class: 'JS-Carousel-Next', text: '>'}),
            Previous: $('<div/>', {class: 'JS-Carousel-Previous', text: '<'}),
            Navigation: $('<ul/>', {class: 'JS-Carousel-Navigation'}),
            Navigation_Item: $('<li>', {class: 'JS-Carousel-Navigation-Item'}),
            Effect: 'transform', // fade | slide | rotate | scale
            Height: 'auto', // no | auto | parent | screen
            Autoscroll: false,
            Index: 0,
            Range: 1,
            Shift: 1,
            Arrows_Active: true,
            Navigation_Active: true,
            Swipe_Active: true,


            // System
            Carousel: $(Carousel),
            Hover: false,
            Default: []
        }, Settings);

        var Rewrite = ['Index', 'Range', 'Shift', 'Arrows_Active', 'Navigation_Active', 'Swipe_Active'];
        $(Rewrite).each(function (Key, Item) {
            Object.Default[Item] = Object[Item];
        });

        // Create Carousel
        Methods.Create = function () {
            if (Object.Count === undefined) {
                Object.Count = Object.Carousel.find(Object.Item).length;
            }

            Object.Animated = false;
            if (Object.Range > Object.Count) {
                Object.Range = Object.Count;
            }
            Object.Item_Width = (100 / Object.Range);

            // Clone Array
            if (Object.Items === undefined) {
                var Items = Object.Carousel.find(Object.Item);
                Object.Items = $.makeArray(Items);
                Items.remove();
            } else {
                $(Object.List).empty();
            }


            // Set items width
            $(Object.Items).css('width', Object.Item_Width + '%');

            // Append Index Item
            var Elements = [],
                Item_Index = 0;

            for (var Index = 0; Index < Object.Range; Index++) {
                if (Object.Index + Index < Object.Count) {
                    Item_Index = Object.Index + Index;
                } else {
                    Item_Index = Object.Index + Index - Object.Count;
                }
                $(Object.Items[Item_Index]).css('left', (Index * Object.Item_Width) + '%');
                Elements.push(Object.Items[Item_Index]);
            }

            Object.List.append($(Elements));


            // Create Arrows
            if (Object.Arrows_Active && Object.Count > Object.Range &&
                $(document).find(Object.Next).length == 0 && $(document).find(Object.Previous).length == 0) {
                Object.Next = Object.Next.clone();
                Object.Previous = Object.Previous.clone();
                Object.Carousel.append([Object.Next, Object.Previous]);
            }


            // Create Navigation
            if (Object.Navigation_Active && Object.Shift == 1 && Object.Range == 1 && Object.Count > 1) {
                $(Object.Navigation).empty();

                for (var Index = 0; Index < (Object.Count / Object.Shift); Index++) {
                    Object.Navigation.append(Object.Navigation_Item.clone());
                }
                Object.Navigation = Object.Navigation.clone();

                Object.Carousel.append(Object.Navigation);

                $($('li', Object.Navigation).get(Object.Index)).addClass('Active')
                    .siblings().removeClass('Active');
            } else {
                $(Object.Navigation).empty();
            }

            Methods.Add_Events();
        };

        // Change current index
        Methods.Change_Index = function (Direction) {
            Object.Actual_Index = Object.Index;
            if (Direction == 'right') {
                if (Object.Index - Object.Shift < 0) {
                    Object.Index = Object.Index - Object.Shift + Object.Count;
                } else {
                    Object.Index = Object.Index - Object.Shift;
                }
            } else if (Direction == 'left') {
                if (Object.Index + Object.Shift >= Object.Count) {
                    Object.Index = Object.Index + Object.Shift - Object.Count;
                } else {
                    Object.Index = Object.Index + Object.Shift;
                }
            } else {
                Object.Index = $(Object.Event.currentTarget).index();
            }

            $($('li', Object.Navigation).get(Object.Index)).addClass('Active')
                .siblings().removeClass('Active');
        };

        // Animate carousel
        Methods.Animate = function (Elements) {

            if (Object.Effect == 'fade') {
                Elements.each(function (Index, Item) {
                    $(Item).css({'left': (Index * Object.Item_Width) + '%'});
                });
                Elements.css({'opacity': 0}).delay(1).promise().done(function () {
                    Elements.css({'opacity': 1});
                    Elements.siblings().not(Elements).css({'opacity': 0});
                });
            } else if (Object.Effect == 'slide') {
                if (Object.Direction == 'right' || (Object.Index < Object.Actual_Index && Object.Direction != 'left')) {
                    Elements.each(function (Index, Item) {
                        $(Item).css({'left': (Index * Object.Item_Width - (100 / Object.Range * Object.Shift)) + '%'});
                    });

                    Elements.delay(1).promise().done(function () {
                        Elements.each(function (Index, Item) {
                            $(Item).css({'left': (Index * Object.Item_Width) + '%'});
                        });

                        Elements.siblings().not(Elements).each(function (Index, Item) {
                            $(Item).css({'left': (Index * Object.Item_Width + 100 ) + '%'});
                        });
                    });
                } else if (Object.Direction == 'left' || Object.Index > Object.Actual_Index) {
                    Elements.each(function (Index, Item) {
                        $(Item).css({'left': (Index * Object.Item_Width + (100 / Object.Range * Object.Shift)) + '%'});
                    });

                    Elements.delay(1).promise().done(function () {
                        Elements.each(function (Index, Item) {
                            $(Item).css({'left': (Index * Object.Item_Width) + '%'});
                        });

                        Elements.siblings().not(Elements).each(function (Index, Item) {
                            $(Item).css({'left': (Index * Object.Item_Width - (100 / Object.Range * Object.Shift)) + '%'});
                        });
                    });
                }
            } else if (Object.Effect == 'rotate') {
                Elements.each(function (Index, Item) {
                    $(Item).css({'left': (Index * Object.Item_Width) + '%'});
                });
                Elements.css({'opacity': 0, 'transform': 'rotateY(0deg)'}).delay(1).promise().done(function () {
                    Elements.css({'opacity': 1, 'transform': 'rotateY(180deg)'});
                    Elements.siblings().not(Elements).css({'opacity': 0, 'transform': 'rotateY(180deg)'});
                });
            } else if (Object.Effect == 'scale') {
                Elements.each(function (Index, Item) {
                    $(Item).css({'left': (Index * Object.Item_Width) + '%'});
                });
                Elements.css({'opacity': 0, 'transform': 'scale(1)'}).delay(1).promise().done(function () {
                    Elements.css({'opacity': 1, 'transform': 'scale(1.3)'});
                    Elements.siblings().not(Elements).css({'opacity': 0, 'transform': 'scale(1)'});
                });
            }
        };

        // Slide
        Methods.Slide = function (Event) {

            // Waiting end of animation
            if (!Object.Animated) {
                Object.Animated = true;
            } else {
                return;
            }

            // Append Element
            Object.Event = Event;

            Methods.Change_Index(Object.Direction);


            // Append Index Item
            var Elements = [],
                Item_Index = 0;

            for (var Index = 0; Index < Object.Range; Index++) {
                if (Object.Index + Index < Object.Count) {
                    Item_Index = Object.Index + Index;
                } else {
                    Item_Index = Object.Index + Index - Object.Count;
                }
                Elements.push(Object.Items[Item_Index]);
            }
            Elements = $(Elements);

            Object.List.append(Elements);

            // Animate
            Methods.Animate(Elements);

            setTimeout(
                function () {
                    $(Elements).siblings().not(Elements).remove();
                    Object.Animated = false;
                },
                parseFloat($(Elements).css('transition-duration')) * 1000);
        };

        // Adaptive
        Methods.Adaptive = function () {
            if (Object.Media) {
                $.each(Rewrite, function (Key, Item) {
                    Object[Item] = Object.Default[Item];
                });

                $.each(Object.Media, function (Key, Item) {
                    var Min_Width = Key.split('_')[1],
                        Max_Width = Key.split('_')[2];

                    if ($(window).width() >= Min_Width && $(window).width() <= Max_Width) {
                        $.each(Item, function (Key, Value) {
                            Object[Key] = Item[Key];
                        });

                    }
                });
            }
            Methods.Create();
        };

        // Auto Height
        Methods.Auto_Height = function () {
            $(Object.List, Object.Carousel).height($(Object.Item, Object.Carousel).actual('innerHeight'));
        };


        if (Object.Height == 'auto') {
            $(window).resize(function () {
                Methods.Auto_Height();
            }).load(function () {
                Methods.Auto_Height();
            });
        }

        // Add Events
        Methods.Add_Events = function () {
            // Events - On Arrows
            if (Object.Arrows_Active) {
                $(Object.Previous).click(function (Event) {
                    Object.Direction = 'left';
                    Methods.Slide(Event);
                });
                $(Object.Next).click(function (Event) {
                    Object.Direction = 'right';
                    Methods.Slide(Event);
                });
            }

            // Events - On Navigation
            if (Object.Navigation_Active) {
                $('li', Object.Navigation).click(function (Event) {
                    Object.Direction = 'navigation';
                    Methods.Slide(Event);
                });
            }

            // Events - On Swipe
            if (Object.Swipe_Active && Object.Count > Object.Range) {
                $(Object.List, Object.Carousel).swipe({
                    swipeLeft: function () {
                        Object.Direction = 'left';
                        Methods.Slide();
                    },
                    swipeRight: function () {
                        Object.Direction = 'right';
                        Methods.Slide();
                    },
                    allowPageScroll: "auto"
                });
            }
        };

        Methods.Adaptive();
        $(window).resize(function () {
            Methods.Adaptive();
        });

        // Auto Scroll
        $(Object.Carousel).hover(function () {
            Object.Hover = true;
        }, function () {
            Object.Hover = false;
        });

        if (Object.Autoscroll && Object.Count > Object.Range) {
            setInterval(function () {
                if (!Object.Hover) {
                    Object.Direction = 'left';
                    Methods.Slide();
                }
            }, Object.Autoscroll);
        }
    });
    return this;
};


$('.Views-Wrap').JS_Easy_Carousel({
//Autoscroll: 4000,
    Effect: 'slide',
    Next: $('<div class="Next"><svg> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow"></use></svg></div>'),
    Previous: $('<div class="Previous"><svg> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow"></use></svg></div>'),
    Navigation: $('<ul/>', {class: 'JS-Carousel-Navigation Views-Nav'})
});