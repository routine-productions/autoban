(function ($) {
    $.fn.TouchButton = function () {
        $(this).each(function () {
            var $Button = $(this);
            console.log($Button);
            var Delay = false;
            $Button.click(function (Event) {
                if (Delay == false) {
                    Delay = true;

                    var $This = $(this);

                    var Spot = {
                        Xpos: Event.pageX - $This.offset().left,
                        Ypos: Event.pageY - $This.offset().top
                    };

                    var $Splash = $('<div class="Splash">');
                    $Splash.appendTo($This).css({
                        'top': Spot.Ypos - ($Splash.height() / 2),
                        'left': Spot.Xpos - ($Splash.width() / 2)
                    });

                    setTimeout(function () {
                        $Splash.remove();
                    }, 1000);

                    setTimeout(function () {
                        Delay = false;
                    }, 500);
                }
            });
        });
    };


    $('.JS-TouchButton').TouchButton();
})(jQuery);


