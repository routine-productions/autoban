/*
 * Copyright (c) 2015
 * Routine JS - Current Link
 * Version 0.1.0
 * Create 2015.12.04
 * Author Bunker Labs

 * Usage:
 *
 * Add class name 'JS-Current-Links' - to Box with navigation
 * Script will add class 'JS-Current-Link' to current link
 * and class 'JS-Current-Link-1' with same first chank

 * Code structure:
 * <div class="JS-Current-Links"><a href='/blog/post'></a></div>
 */
(function ($) {
    $(document).ready(function () {
        var $Current_Url    = $('.JS-Current-Links a'),
            Current_Link    = 'Active',
            Current_Chunk_1 = 'JS-Current-Link-1',
            Current_Chunk_2 = 'JS-Current-Link-2';

        $Current_Url.each(function () {
            var Link            = $(this).attr('href').replace(location.origin).replace(location.host);
            var Link_Chunks     = Link.split('/');
            var Location_Chunks = location.pathname.split('/');


            if (Link == location.pathname) {
                //$(this).addClass(Current_Link);
                $(this).parent( ).addClass(Current_Link);
                $(this).removeAttr('href');
            }

        });
    });
})(jQuery);