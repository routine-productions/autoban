(function ($) {
    $(document).ready(function () {

        $('.JS-Scroll-Button').click(function () {

            var Scroll_Point_Duration_String = $(this).attr('data-scroll-point-duration') ? $(this).attr('data-scroll-point-duration') : 800,
                Scroll_Point_Duration = parseInt(Scroll_Point_Duration_String);

            var Id = $(this).attr('href').replace('/','');
            var Scroll_Position = $(Id).offset().top;



            if (location.pathname == '/') {
                $('body,html').animate({
                    scrollTop: Scroll_Position
                }, Scroll_Point_Duration);
            }


            return false;
        });
    });
})(jQuery);


